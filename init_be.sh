#!/bin/bash

#
#  init_be.sh
#
#  Copyright (c) 2017-2018 Decorum Development Team <p01arst0rm@github.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#
# This is the uninstallation script used to start the decorum build
# environment.
#


export DIR=$(pwd)
status="1"

git_update() {
    echo "attempting to update build environment.."
    git pull --rebase origin master    
    status=1
}

main() {
    case $status in
        # start/restart the build environment
        1) 
            sh $DIR/_include/be_main.sh $@
            status="$?"
            main;;
        # Update the build environment
        2) 
            sh $DIR/_include/be_main.sh $@
            status="$?"
            main;;
        # exit the build environment
        0|*) 
            exit 0;;
    esac


}



# terminal entry
# ------------------------------------------------------------
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  main
fi
