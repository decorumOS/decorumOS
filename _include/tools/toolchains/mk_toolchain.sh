#!/bin/bash

#
#  _include/mk/mk_toolchain.sh
#
#  Copyright (c) 2017-2018 Decorum Development Team <p01arst0rm@github.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#
# This script is used to build toolchains used by decorum 
# initialiser
#

mk_toolchain_symlinks() {
  mkdir $DIR/tcsrc/
  ln -s $DIR/tcsrc/ $DIR/
  ln -s $DIR/tcsrc/ $DIR/
}







