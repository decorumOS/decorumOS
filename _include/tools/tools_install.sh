#!/bin/bash

#
#  tools/tools/tools_install.sh
#
#  Copyright (c) 2017-2018 Decorum Development Team <p01arst0rm@github.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


get_crosstool_ng() {
  cd $DIR/src/tools
  addr="https://github.com/crosstool-ng/crosstool-ng"
  echo "Checking crosstool-ng repo is alive.."
  x=$(check_connect $addr)

  if [ $x == "ok" ]; then
    git clone $addr
    cd crosstool-ng
    git checkout origin/master
  else
    connect_err "crosstool-ng repo"
  fi

}


build_crosstool_ng() {
  cd $DIR/src/tools/crosstool-ng
  ./bootstrap
  ./configure --prefix=$DIR/tools/
  make
  make install
}


get_sqlite() {
  cd $DIR/src/tools
  addr="https://www.sqlite.org/src/tarball/sqlite.tar.gz?r=release"
  echo "Checking sqlite is alive.."
  x=$(check_connect $addr)

  if [ $x == "ok" ]; then
    wget -O sqlite.tar.xz $addr
    tar -xf sqlite.tar.xz
    rm sqlite.tar.xz
  else
    connect_err "sqlite tar mirror"
  fi

}


build_sqlite() {
  cd $DIR/src/tools/sqlite
}


get_tools() {
  rm -rf $DIR/src/tools
  mkdir $DIR/src/tools
  get_crosstool_ng
  get_sqlite
}


build_tools() {
  build_crosstool_ng
  build_sqlite
}


install_tools() {
  clear

  get_tools
  #build_tools

  echo "Tools installed. Press any key to continue."
  read -n 1 -s
}
