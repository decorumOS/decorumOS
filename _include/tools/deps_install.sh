#!/bin/bash

#
#  _include/deps_install.sh
#
#  Copyright (c) 2017-2018 Decorum Development Team <p01arst0rm@github.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#
# This script is used to install dependancies used by the decorum init
# environment.
#

install_deps_mida() {
  mida -u
  mida -if decorum-base
  mida -if init-deps
}

rem_deps_mida() {
  mida -u
  mida -r init-deps
}

install_deps_apt() {
  apt-get update -y
  apt-get upgrade -y

  i=(
  "bash" "binutils" "bison"
  "coreutils" "diffutils"
  "findutils" "gawk" "gcc"
  "grep" "gzip" "m4" "xz" "tar"
  "make" "patch" "perl"
  "sed" "texinfo" "automake"
  "subversion" "git" "flex"
  "help2man" "libtool"

  "linux-source" "linux-headers-$(uname -r)"
  "libc-dev-bin" "libc-bin" "glibc-source"
  "g++" "python-dev"
  )

  for a in "${i[@]}"; do
    apt-get -y install $a
  done
}

install_deps_pacman() {
  Pacman -Syu
  Pacman -Sy 'base-devel'

  i=(
  "bash" "binutils" "bison"
  "coreutils" "diffutils"
  "findutils" "gawk" "gcc"
  "grep" "gzip" "m4" "xz" "tar"
  "make" "patch" "perl"
  "sed" "texinfo" "automake"
  "subversion" "git" "flex"
  "help2man" "libtool"

  "linux" "linux-headers"
  "glibc" "ncurses"
  "g++" "python"
  )

  for a in "${i[@]}"; do
    Pacman -Sy $a
  done
}


install_deps_yum() {
  yum -y update
  yum -y groupinstall 'Development Tools'

  i=(
  "bash" "binutils" "bison"
  "coreutils" "diffutils"
  "findutils" "gawk" "gcc"
  "grep" "gzip" "m4" "xz" "tar"
  "make" "patch" "perl"
  "sed" "texinfo" "automake"
  "subversion" "git" "flex"
  "help2man" "libtool"

  "kernel-devel" "kernel-headers"
  "gcc-c++" "glibc" "ncurses-devel"
  "ncurses-libs" "python-devel"
  )


  for a in "${i[@]}"; do
    yum -y install $a
  done
}

install_deps_dnf() {
  dnf -y update
  dnf -y groupinstall 'Development Tools'

  i=(
  "bash" "binutils" "bison"
  "coreutils" "diffutils"
  "findutils" "gawk" "gcc"
  "grep" "gzip" "m4" "xz" "tar"
  "make" "patch" "perl"
  "sed" "texinfo" "automake"
  "subversion" "git" "flex"
  "help2man" "libtool"

  "kernel-devel" "kernel-headers"
  "gcc-c++" "glibc" "ncurses-devel"
  "ncurses-libs" "python-devel"
  )


  for a in "${i[@]}"; do
    dnf -y install $a
  done
}

install_deps_pkg() {
  clear
  echo "installing generic dependancies.."
  case $build_pm in
    1)
      install_deps_mida;;
    2)
      install_deps_apt;;
    3)
      install_deps_pacman;;
    4)
      install_deps_yum;;
    5)
      install_deps_dnf;;
    *)
      menu_build_pm
      install_deps_pkg;;
  esac
}

install_deps_gener() {
  clear
  echo "installing generic dependancies.."
  mkdir -v $dir_root
  mkdir -v $tools
  ln -sv $tools /
  mkdir -v $root/sources
  groupadd decinit
  useradd -s /bin/bash -g decinit -m -k /dev/null decinit
  cp -r $DIR/_include/config/bash/bash_profile /home/decinit/.bash_profile
  cp -r $DIR/_include/config/bash/bashrc /home/decinit/.bashrc
}

rem_deps_gener() {
  userdel -r decinit
  groupdel decinit
  rm -rf /home/decinit
}

install_deps() {
  clear
  install_deps_gener
  install_deps_pkg
  echo "Dependancies installed. Press any key to continue."
  read -n 1 -s
}

rem_deps() {
    echo  ""
}
