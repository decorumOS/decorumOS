#!/bin/bash

#
#  _include/be_alert.sh
#
#  Copyright (c) 2017-2018 Decorum Development Team <p01arst0rm@github.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#
# This script contains error, warning and notfiy cases that
# may occur during the usage of the build environment
#


# errors
# ------------------------------------------------------------

error_throw() {
  local error_text="# [ERROR]: ${1}"
  local error_type=$2
  echo "${error_text}"
  exit 3
}

err_unknown_param() {
	error_throw ""
}

# warnings
# ------------------------------------------------------------

warn_throw() {
  local warn_text="# [WARNING]: ${1}"
  echo "${warn_text}"
}

# notifies
# ------------------------------------------------------------

notify_usage="\

------------------------------------------------------------

-v, --version    Show Decorum Init Version
-h, --help       Show Decorum Init usage menu

------------------------------------------------------------
COMPILER OPTIONS

--host_arch      Choose Host device architecture
                 (will attempt to use uname to determine
                 automatically)

--host_pm        Choose host package manager

--build_arch     Set build architecture
                 ( If you are building on an external
                 device. NOT YET SUPPORTED.)

--build_pm       Choose build package manager

--target_arch    Choose Target device architecture

--target_pm      Choose host package manager

------------------------------------------------------------

"

notify_install="\

build version: $dec_vers
target envrionment: $target_env

decorum root target: $root_mnt
decorum home target: $home_mnt
decorum boot target: $boot_mnt
decorum install target: $install_mnt

build architecture: $build_arch
build Package manager: $build_pm
host architecture: $host_build
host Package manager: $host_pm

do you wish to continue? [y/n]

"
