#!/bin/bash

#
#  _include/be_include.sh
#
#  Copyright (c) 2017-2018 Decorum Development Team <p01arst0rm@github.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#
# This is the include script to bring all init dependancies into
# the init environment
#

# Add init tools to PATH
# ------------------------------------------------------------
export PATH="$PATH:$DIR/tools/bin"

# build environment main scripts
# ------------------------------------------------------------
source $DIR/_include/be_config.sh
source $DIR/_include/be_alert.sh
source $DIR/_include/be_macros.sh

# tool scripts
# ------------------------------------------------------------
source $DIR/_include/tools/deps_install.sh
source $DIR/_include/tools/tools_install.sh



# build scripts
# ------------------------------------------------------------
#source $DIR/_include/build.sh
#source $DIR/_include/build_deps.sh

# build environment ui
# ------------------------------------------------------------
source $DIR/_include/ui/ui_headers.sh
source $DIR/_include/ui/ui_menus.sh
source $DIR/_include/ui/ui_options.sh

