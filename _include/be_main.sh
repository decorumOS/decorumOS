#!/bin/bash

#
#  _include/be_main.sh
#
#  Copyright (c) 2017-2018 Decorum Development Team <p01arst0rm@github.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#
# This is the main decorum init script for creating a decorum linux
# installation. Execute this to start the installation processs.
#




# generic variables
# ------------------------------------------------------------
export install=0
export be_vers="1.0"
export dec_vers=""
export dir_root="/mnt/dfs"


# include dependancies
source ${DIR}/_include/be_include.sh


# build variables
# ------------------------------------------------------------
# this is the architecture decorum
# is being built on
build_arch=$(uname -r)
build_distro=""
build_pm=""

# this is the architecture on which you
# will be installing/running decorum
host_arch=""
host_pm=""
host_env=""

# this is where installation tools will be built to
tools="/mnt/dfs/tools"


# install variables
# ------------------------------------------------------------
# "/" host
root=""
# "/home" host
home=""
# "/boot" host
boot=""
# "/update" host
update=""




# main
# ------------------------------------------------------------
parse_opts() {
    while [ ${#} -gt 0 ]; do
        case ${1} in
            -v|--version)
                version;;
            -h|--help)
                usage;;
            -i=*|--host_build=*)
                host_build="${1#*=}"
                shift;;
            -i|--host_build)
                host_build="${2}"
                shift && shift;;


        *)
          err_unknown_param "${1}";;
      esac
    done
}



parse_opts() {
    while [ ${#} -gt 0 ]; do
        case ${1} in
            -v | --version)
                version;;
            -h | --help)
                usage;;
            -i | --install)
                echo "direct installation selected."
                install=1
                ;;
            --build_build=*)
                build_build="${i#*=}";;
            --host_build=*)
                host_build="${i#*=}";;
            --build_pm=*)
                build_pm="${i#*=}";;
            --host_pm=*)
                host_pm="${i#*=}";;
      esac
    done
}


main(){
    check_root
    if [ $install -eq 1 ]; then
        dec_inst
    else
        menu_load
    fi
    exit 0
}



parse_opts $@ && main