#!/bin/bash

#
#  tools/be_macros.sh
#
#  Copyright (c) 2017-2018 Decorum Development Team <p01arst0rm@github.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#
# This script is is a collection of generic functions used by the init
# utility in various different other scripts.
#

check_root() {
    if [ $EUID -ne 0 ]; then
        echo "ERROR: Decorum Init must be run as root"
        exit 0
    fi
}

connect_err() {
  echo "WARNING: cannot connect to $1. Press any key to continue."
  read -n 1 -s
}

invalid_input() {
  echo "Invalid option. Press any key to continue."
  read -n 1 -s
}

check_connect() {
  if curl -s --head  --request GET "$1" | grep "200 OK" > /dev/null; then
     echo "ok"
  else
     echo "err"
  fi
}


pkg_info() {
# Print package information
echo "$installMessage"
echo "\
Host Repo:    $host_repo
Pkg Name:     $pkg_name
Pkg Version:  $pkg_version"
}
