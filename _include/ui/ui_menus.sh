#!/bin/bash

#
#  _include/ui/ui_menus.sh
#
#  Copyright (c) 2017-2018 Decorum Development Team <p01arst0rm@github.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#
# These are the UI menus used by decorum init
#


# information menus
# --------------------------------------------

version() {
  echo "init version: $vers"
  echo "decorum build: $dec_vers"
  exit
}

usage() {
  notify_usage
  exit
}



# build menus
# --------------------------------------------

menu_build_dist() {
  clear
  echo "$header_dist$options_dist"
  read option

  case $option in
    0)
      return;;
    [1-9])
      build_distro=$option
      return;;
    *)
      invalid_input;;
    esac
  menu_build_dist
}

menu_build_pm() {
  clear
  echo "$header_pm$options_pm"
  read option

  case $option in
    0)
      return;;
    [1-9])
      build_pm=$option
      return;;
    *)
      invalid_input;;
    esac
  menu_build_dist
}

# host menus
# --------------------------------------------






# main menus
# --------------------------------------------


menu_env() {
  clear
  echo "$header_decinit$options_decinit"
  read option

  case $option in
    0)
      return;;
    1)
      install_deps;;
    2)
      install_tools;;
    *)
      invalid_input;;
  esac
  menu_env
}

menu_build() {
  clear
  echo "$header_build$options_build"
  read option

  case $option in
    0)
      return;;
    1)
      menu_build_dist;;
    2)
      menu_build_pm;;
    *)
      invalid_input;;
  esac
  menu_build
}


menu_host() {
  clear
  echo "$header_host$options_host"
  read option

  case $option in
    0)
      return;;
    *)
      invalid_input;;
  esac
  menu_host
}

menu_config() {
  clear
  echo "$header_config$options_config"
  read option

  case $option in
    0)
      return;;
    1)
      # kernel config
      return;;
    2)
      # select init
      return;;
    3)
      # select shell
      return;;
    4)
      # set hostname
      return;;
    *)
      invalid_input;;
  esac
  menu_config
}

menu_load() {
  clear
  echo "$header_load$options_load"
  read option

  case $option in
    0)
      clear
      exit 0;;
    1)
      menu_env;;
    2)
      menu_build;;
    3)
      menu_host;;
    4)
      menu_config;;
    *)
      invalid_input;;
  esac
  menu_load
}
