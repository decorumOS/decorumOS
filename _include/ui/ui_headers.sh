#!/bin/bash

#
#  _include/ui/ui_headers.sh
#
#  Copyright (c) 2017-2018 Decorum Development Team <p01arst0rm@github.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


header_load="\
-------------------------------------------------------------------------
|
| Welcome to the decorum initialiser script
| Copyright (c) 2017-2018 Decorum Development Team
| <p01arst0rm@github.com>
|
-------------------------------------------------------------------------

"

header_pm="\
-------------------------------------------------------------------------
|
| Please specify your current package manager
|
-------------------------------------------------------------------------

"
header_dist="\
-------------------------------------------------------------------------
|
| Please specify your current distribution
|
-------------------------------------------------------------------------

"



header_set_var() {

a="\
-------------------------------------------------------------------------
|
| Please enter desired variable for $1
| Press return to leave unchanged
|
-------------------------------------------------------------------------

  "
  echo "$a"
}

header_decinit="\
-------------------------------------------------------------------------
|
| Configure your decorum installation
|
-------------------------------------------------------------------------

"

header_config="\
-------------------------------------------------------------------------
|
| Configure your decorum installation
|
-------------------------------------------------------------------------

"

header_build="\
-------------------------------------------------------------------------
|
| Configure your build environment
|
-------------------------------------------------------------------------

"
header_host="\
-------------------------------------------------------------------------
|
| Configure your host environment
|
-------------------------------------------------------------------------

"
