#!/bin/bash

#
#  _include/ui/ui_options.sh
#
#  Copyright (c) 2017-2018 Decorum Development Team <p01arst0rm@github.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#



# build options
# ------------------------------------------------------------
options_dist="\
 1. Decorum
 2. Debian
 3. Ubuntu
 4. Kerbuntu
 5. Arch

 6. Arch_Strike
 7. Redhat
 8. Fedora
 9. Centos
 10. Linux from source

 0. Return
"

# config options
# ------------------------------------------------------------
options_pm="\
 1. mida      Decorum
 2. apt       Debian / Ubuntu / Kerbuntu
 3. pacman    Arch / Arch Strike
 4. yum       Redhat / Fedora / Centos
 5. dnf       Fedora
 6. Zypper    openSUSE
 7. ALPM      Alipine Linux
 8. na        no package manager

 0. Return
"


options_Kernel="\
1. defconfig    Compile a kernel best suited for me
2. mvconfig     I already have a compiled kernel

3. menuconfig   I want to set up the kernel myself
4. noah         I want to use the XNU kernel

0. Return
"

options_shell="\
1. bash
2. Tcsh
3. Ksh
4. Zsh
5. Fish

0. Return
"

options_init="\
1. SysV Init
2. SystemD
3. OpenRC
4. Runit

0. Return
"

# main options
# ------------------------------------------------------------
options_decinit="\
 1. Install dependancies
 2. Install environment tools
 3. Add repo

 0. Return
"

options_build="\
 1. Choose build distribution
 2. Choose build package manager

 0. Return
"

options_host="\



 0. Return
"

options_config="\
 1. Choose kernel config
 2. Choose init
 3. Choose Shell
 4. Set hostname

 0. Return
"

options_load="\
 1. Init options
 2. Build options
 3. Host options

 5. Configure Decorum installation
 6. Install decorum

 0. Exit
"
