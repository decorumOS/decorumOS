# Welcome to Decorum

This repo holds all relevent code for the decorum linux, the official linux distribution of Auris.

Current decorum version:        1.0 Alpha
Current initialiser version:    1.0

== FAQ == 

-- $thing is broken --

99% chance we know. most things are WIP/POC and are done quick and dirty to get something working. if you know of a better way of doing something, why not contribute?

-- Will decorum linux ever use systemd? --

will the letter "e" stop existing? probably not. Well unless you want that.

-- I have this awesome idea for a feature! --

cool. POC || GTFO.

-- A package i want to install just has a null file? --

This package is not yet officially supported as it has not been added/ configured to work with the init script. it is possible that you can install this package once you have a working installation, though.

-- what devices are supported?

information on all currently supported devices is available in _resources/devices/ . there are generic patches 
for different architectures, and in future, we will have patch scripts that set up the repository for your specific device.

-- my device/architecture isnt supported? --

If you want support for your device, consider researching how to build these packages for it, or offer up device sacrifices to the developer alter so we can do it for you.

